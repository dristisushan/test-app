import 'package:smartmobe_test_app/base/utilities.dart';
import 'package:smartmobe_test_app/model/Hotel/resultMap.dart';

class Hotel {
  int albumId;
  int id;
  String title;
  String url;
  String thumbnailUrl;

  Hotel.fromResult(ResultMap item)
      : albumId = item.getInt("albumId"),
        id = item.getInt("id"),
        url = item.getString("url") ?? "",
        title = item.getString("title") ?? "",
        thumbnailUrl = item.getString("thumbnailUrl") ?? "";

  static Hotel init() => Hotel.fromJson({});
  static Hotel fromJson(Map map) => Hotel.fromResult(ResultMap(map));
  static Hotel fromString(String jstr) =>
      fromJson(Utilities.decodeJson(jstr));
  static List fromJsonArray(jarr) =>
      (jarr as List).map((i) => fromJson(i)).toList();
  static List fromJsonString(String jstr) =>
      fromJsonArray(Utilities.decodeJson(jstr));
}