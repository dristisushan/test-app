class ResultMap {
  final Map map;
  ResultMap(this.map);
  dynamic get(String key) => map[key];
  String getString(String key) => map[key]?.toString() ?? "";
  String getStringWithFallback(String key, String fallback) =>
      map[key]?.toString() ?? fallback;
//  List getList(String key)=>map[key];
  double getDouble(String key) => double.parse(getStringWithFallback(key, "0"));
  int getInt(String key) => int.parse(getStringWithFallback(key, "0"));
  bool getBool(String key) => getString(key) == "1" || getString(key) == "true";
}