import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:smartmobe_test_app/base/colors.dart';

class Utilities {
  static const bool DEBUG = kReleaseMode?false:true;


  static doubleBack(BuildContext context){
    Utilities.closeActivity(context);
    Utilities.closeActivity(context);
  }

  static fadeInImage({double height, double width, String image}) {
    return FadeInImage.assetNetwork(
      width: width,
      height: height,
      placeholder: "assets/images/logo_sticky1.png",
      image: image,
      fit: BoxFit.cover,
    );
  }

  static double screenWidth(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return screenWidth;
  }

  static double screenHight(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return screenHeight;
  }

  static log(String message) {
    if (DEBUG) print(message);
  }

  static Future<Null> openActivity(context, object) async {
    return await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => object),
    );
  }

  static Future<Null> fadeOpenActivity(context, object) async {
    return await Navigator.of(context).push(
      new PageRouteBuilder(
        pageBuilder: (BuildContext context, _, __) {
          return object;
        },
        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
          return FadeTransition(opacity: animation, child: child);
        },
      ),
    );
  }

  static Future<Null> fadeReplaceActivity(context, object) async {
    return await Navigator.of(context).pushReplacement(
      new PageRouteBuilder(
        pageBuilder: (BuildContext context, _, __) {
          return object;
        },
        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
          return FadeTransition(opacity: animation, child: child);
        },
      ),
    );
  }

  static void replaceActivity(context, object) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => object),
    );
  }



  static void removeStackActivity(context, object) {
    Navigator.pushAndRemoveUntil(
        context, MaterialPageRoute(builder: (context) => object), (r) => false);
  }

  static void closeActivity(context) {
    Navigator.pop(context);
  }

  static String encodeJson(dynamic jsonData) {
    return json.encode(jsonData);
  }

  static dynamic decodeJson(String jsonString) {
    return json.decode(jsonString);
  }


  static Widget widgetWithLoading({bool visible: true, Widget child}) {
    return Stack(children: <Widget>[
      child ?? Container(),
      Visibility(
          visible: visible,
          child: Container(
            color: Colors.black26,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ))
    ]);
  }

  static bool isIOS() {
    return Platform.isIOS;
  }


  static Future<bool> exitApp(BuildContext context) {
    return showDialog(
      context: context,
      child: new AlertDialog(
        title: new Text(
          'Do you want to exit this application?',
          style: TextStyle(fontSize: 18),
        ),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No', style: TextStyle(fontSize: 15)),
          ),
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text('Yes', style: TextStyle(fontSize: 15)),
          ),
        ],
      ),
    ) ??
        false;
  }

  static void showToast({message, type}) {
    if (type == null) {
      Fluttertoast.showToast(msg: message, toastLength: Toast.LENGTH_LONG);
    } else if (type == 'success') {
      Fluttertoast.showToast(
          msg: message,
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 4,
          backgroundColor: Colors.green,
          textColor: AppColors.whiteColor,
          fontSize: 13.0);
    } else if (type == 'loading') {
      Fluttertoast.showToast(
          msg: message,
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 4,
          backgroundColor: Color(0xFFf68c38),
          textColor: Colors.white,
          fontSize: 13.0);
    } else {
      Fluttertoast.showToast(
          msg: message,
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 4,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 13.0);
    }
  }
}