
import 'package:flutter/material.dart';

class AppColors{
  static const Color gradientGrayColor = Color(0xfff2f3f4);
  static const Color whiteColor = Colors.white;
  static const Color shadowGrayColor = Color(0xff65bcda);
  static const Color grayColor = Color(0xffbababa);
  static const Color redColor = Color(0xfffe8d8d);
  static const Color darkGrayColor = Color(0xffe5e7ea);
  static const Color blueShadowColor = Color(0xffeaecf0);
  static const Color lightBlack = Color(0xff636468);
}