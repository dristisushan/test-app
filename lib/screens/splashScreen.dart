import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:smartmobe_test_app/base/utilities.dart';
import 'HomeScreen/index.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    // TODO: implement initState
    Future.delayed((Duration(milliseconds: 4700)),(){
      Utilities.openActivity(context,HomeScreen());
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Lottie.asset('assets/lottie/splash.json'),
      ),
    );
  }
}
