import 'package:flutter/material.dart';
import 'package:smartmobe_test_app/base/colors.dart';

class BottomSection extends StatelessWidget {
  const BottomSection({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 3,
          child: Padding(
            padding: const EdgeInsets.only(right:8.0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: AppColors.grayColor.withOpacity(0.3)
              ),
              child:Padding(
                padding: const EdgeInsets.symmetric(vertical:13.0,horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(Icons.remove,color: AppColors.grayColor,),
                    Text("1H",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w600,color: Colors.black),),
                    Icon(Icons.add,color: AppColors.redColor,),
                  ],
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 4,
          child: Padding(
            padding: const EdgeInsets.only(left:0.0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.black
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical:15.0,horizontal: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Add",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w600,color: AppColors.whiteColor),),
                    Text("\$15,00",style: TextStyle(fontSize: 15,fontWeight: FontWeight.w500,color: AppColors.whiteColor),)
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
