
import 'package:flutter/material.dart';
import 'package:smartmobe_test_app/base/colors.dart';

class Description extends StatelessWidget {
  final String title;

  const Description({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 12.0),
          child: Text(
            "Meeting Place",
            style: TextStyle(
                fontSize: 22, fontWeight: FontWeight.w600),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 12.0),
          child: Text(
            "360 Street, NY",
            style: TextStyle(
                color: AppColors.grayColor,
                fontSize: 16,
                fontWeight: FontWeight.w400),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 15.0),
          child: Text(
            "$title Lorem ipsum dolor sit amet,"
                " consectetur adipiscing elit,"
                " sed do eiusmod tempor incididunt"
                " ut labore et dolore magna aliqua.",
            style: TextStyle(
                color: AppColors.grayColor,
                fontSize: 16,
                fontWeight: FontWeight.w400),
          ),
        ),
      ],
    );
  }
}
