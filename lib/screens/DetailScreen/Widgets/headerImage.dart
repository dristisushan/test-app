
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:smartmobe_test_app/base/utilities.dart';

class HeaderImage extends StatelessWidget {
  final String image;

  const HeaderImage({Key key, this.image}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: Utilities.screenWidth(context) * 0.9,
      width: Utilities.screenWidth(context),
      child: Padding(
        padding: const EdgeInsets.all(13.0),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(25),
          child:CachedNetworkImage(
            fit: BoxFit.cover,
            height: 100,
            width: 100,
            placeholder: (context, url) => Lottie.asset('assets/lottie/imageLoading.json'),
            // imageUrl: image,
            imageUrl: "https://bit.ly/3sWgyf1",
          ),
        ),
      ),
    );
  }
}
