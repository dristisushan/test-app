import 'package:flutter/material.dart';
import 'package:smartmobe_test_app/base/colors.dart';

class FacilityCart extends StatelessWidget {
  final Icon icon;
  final String name;

  const FacilityCart({Key key, this.icon, this.name}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right:10.0),
      child: Container(
        width: 80,
        decoration:BoxDecoration(borderRadius: BorderRadius.circular(15),border: Border.all(color:AppColors.grayColor)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [Padding(
            padding: const EdgeInsets.only(bottom:4.0),
            child: icon,
          ), Text(name)],
        ),
      ),
    );
  }
}
