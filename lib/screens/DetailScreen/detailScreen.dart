import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smartmobe_test_app/base/colors.dart';
import 'package:smartmobe_test_app/base/utilities.dart';
import 'package:smartmobe_test_app/bloc/hotelBloc/bloc.dart';
import 'package:smartmobe_test_app/model/Hotel/hotel_model.dart';
import 'package:smartmobe_test_app/screens/DetailScreen/index.dart';

class DetailScreen extends StatefulWidget {
  final int id;

  const DetailScreen({Key key, this.id}) : super(key: key);
  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  var hotelDetail;
  @override
  void initState() {
    // TODO: implement initState
    List<Hotel> data = BlocProvider.of<HotelBloc>(context).hotelList;
    Utilities.log("this is data ${widget.id}");
    Utilities.log("this is data $data");

    hotelDetail = data.where((data)=>data.id == widget.id).toList()[0];
    Utilities.log("this is data $data");
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                stops: [0.2, 0.4],
                colors: [AppColors.darkGrayColor, Colors.white])),
        child: ListView(
          children: [
            HeaderImage(image: hotelDetail.url,),
            Container(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Description(title: hotelDetail.title,),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15.0),
                      child: Text(
                        "Facilities",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 18.0),
                      child: Container(
                        height: 80,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: [
                            FacilityCart(icon:Icon(Icons.wifi),name:"Wifi"),
                            FacilityCart(icon:Icon(Icons.fastfood),name:"Food"),
                            FacilityCart(icon:Icon(Icons.tv),name:"TV"),
                            FacilityCart(icon:Icon(Icons.accessibility_new_sharp),name:"Gym"),
                            FacilityCart(icon:Icon(Icons.music_note),name:"Live Music"),
                          ],
                        ),
                      ),
                    ),

                    BottomSection(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

}

