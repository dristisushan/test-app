import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:smartmobe_test_app/base/colors.dart';
import 'package:smartmobe_test_app/base/utilities.dart';
import 'package:smartmobe_test_app/bloc/hotelBloc/bloc.dart';
import 'package:smartmobe_test_app/model/Hotel/hotel_model.dart';
import 'index.dart';


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HotelBloc _hotelBloc;
  List<Hotel> hotelList = [];
  int _apiPage = 1;
  bool _processApi = false;
  ScrollController _scrollController = ScrollController();

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    double maxScroll = _scrollController.position.maxScrollExtent;
    double currentScroll = _scrollController.position.pixels;

    if (currentScroll == maxScroll) {
      if (!_processApi) {
        _hotelBloc.add(
            GetHotels(_apiPage.toString(),"10"));
        _processApi = true;
      }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    _hotelBloc = BlocProvider.of<HotelBloc>(context);
    _hotelBloc.add(GetHotels(_apiPage.toString(), "10"));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _scrollController.addListener(_onScroll);
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: homeAppBar(),
      body: BlocListener<HotelBloc,HotelState>(
        bloc: _hotelBloc,
        listener: (context,state){
          if (state is HotelStateError) {
            Utilities.showToast(
                type: 'error', message: state.errorMessage);
          }
          if (state is GetHotelStateSuccess) {
            if(state.hotelData.isNotEmpty){
              _apiPage +=1;
              hotelList.addAll(state.hotelData);
            }
          }
          _processApi = false;
        },
        child: BlocBuilder<HotelBloc,HotelState>(
          builder: (context,state){
            if(state is HotelStateError){
              return Center(child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Error occurred",style: TextStyle(color: Colors.red,fontSize: 22,),),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: IconButton(icon: Icon(Icons.refresh), onPressed: (){
                      _hotelBloc = BlocProvider.of<HotelBloc>(context);
                    _hotelBloc.add(GetHotels(_apiPage.toString(), "10"));}),
                  )
                ],
              ));
            }else {
              if( hotelList.length == 0 && _apiPage == 1){
                return Center(
                  child: Container(
                    height: 30,
                    width: 30,
                    child: CircularProgressIndicator(),
                  ),
                );
              } else if(state is GetHotelStateSuccess && state.hotelData.length == 0 && _apiPage == 1){
                return Center(child: Text("No Data found"),);
              }else{
                return Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Text(
                            "Meeting\nPlace",
                            style: TextStyle(fontSize: 42, fontWeight: FontWeight.w800),
                          ),
                        ),
                        Container(
                          height: 20,
                        ),
                        Expanded(
                          child: ListView.builder(
                              controller: _scrollController,
                              physics: AlwaysScrollableScrollPhysics( parent: BouncingScrollPhysics() ),
                              itemCount: hotelList.length,
                              itemBuilder: (context, index) {
                                return  CardView(index: index,color: AppColors.shadowGrayColor,hotel: hotelList[index]);
                              }),
                        ),
                      ],
                    ),
                  ),
                );
              }
            }
          },
        )
      )
    );
  }
}

