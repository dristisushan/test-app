import 'dart:developer';
import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:smartmobe_test_app/base/colors.dart';
import 'package:smartmobe_test_app/base/utilities.dart';
import 'package:smartmobe_test_app/model/Hotel/hotel_model.dart';
import 'package:smartmobe_test_app/screens/DetailScreen/index.dart';

class CardView extends StatelessWidget {
  final int index;
  final Color color;
  final Hotel hotel;

  const CardView({Key key, this.index, this.color, this.hotel})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          bottom: 10.0, left: 20, right: 20, top: index == 0 ? 0 : 10),
      child: InkWell(
        onTap: () {
          Utilities.openActivity(context, DetailScreen(id: hotel.id,));
        },
        child: Container(
          decoration: BoxDecoration(
              color: index == 0 ? Colors.white : null,
              boxShadow: [
                BoxShadow(
                  color: AppColors.blueShadowColor,
                  blurRadius: 25.0,
                  spreadRadius: 5.0,
                  offset: Offset(
                    5.0,
                    15.0,
                  ),
                )
              ],
              borderRadius: BorderRadius.circular(15),
              gradient: index == 0
                  ? null
                  : LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      stops: [0.1, 0.4],
                      colors: [AppColors.gradientGrayColor, AppColors.whiteColor])),
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Container(
              height: 110,
              child: Row(
                children: [
                  Expanded(
                    flex: 4,
                    child: Container(
                      width: 115,
                      child: Stack(
                        children: [
                          Positioned(
                            top: 0,
                            left: 0,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(25),
                              child:
                              CachedNetworkImage(
                                fit: BoxFit.cover,
                                height: 100,
                                width: 100,
                                placeholder: (context, url) => Lottie.asset('assets/lottie/imageLoading.json'),
                                // imageUrl: hotel.thumbnailUrl,
                                imageUrl: "https://bit.ly/3sWgyf1",
                              ),
                            ),
                          ),
                          Positioned(
                            bottom: 0,
                            right: 0,
                            child: Container(
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    //background color of box
                                    BoxShadow(
                                      color: color.withOpacity(0.2),
                                      blurRadius: 15.0, // soften the shadow
                                      spreadRadius: 5.0, //extend the shadow
                                      offset: Offset(
                                        5.0, // Move to right 10  horizontally
                                        5.0, // Move to bottom 10 Vertically
                                      ),
                                    )
                                  ],
                                  borderRadius: BorderRadius.circular(20),
                                  color: AppColors.shadowGrayColor),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8, vertical: 10),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.favorite,
                                      color: AppColors.whiteColor,
                                    ),
                                    Text(
                                      "12 k",
                                      style: TextStyle(color: AppColors.whiteColor),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                      flex: 6,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 8.0, left: 15),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "\$12",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 23,
                                    fontWeight: FontWeight.w600),
                              ),
                              Text(
                                hotel.title,
                                style: TextStyle(
                                    color: AppColors.grayColor,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400),
                                overflow: TextOverflow.ellipsis,
                                softWrap: true,
                                maxLines: 1,
                              ),
                              Text(
                                "320 sq.ft   | 4BHK",
                                style: TextStyle(
                                    color: AppColors.lightBlack,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                        ),
                      )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
