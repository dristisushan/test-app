import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

homeAppBar(){
  return AppBar(
    backgroundColor: Colors.transparent,
    elevation: 0,
    automaticallyImplyLeading: false,
    leading: Padding(
      padding: const EdgeInsets.only(left: 10.0),
      child: Icon(
        Icons.arrow_back_ios,
        color: Colors.black,
      ),
    ),
    actions: [
      IconButton(
          icon: Icon(
            CupertinoIcons.search,
            color: Colors.black,
          ),
          onPressed: () {}),
      Padding(
        padding: const EdgeInsets.only(right: 10.0),
        child: IconButton(
            icon: Icon(
              CupertinoIcons.square_grid_2x2_fill,
              color: Colors.black,
            ),
            onPressed: () {}),
      )
    ],
  );
 }