import 'package:meta/meta.dart';

@immutable
abstract class HotelEvent {}

class GetHotels extends HotelEvent {
  final String page,limit;
  GetHotels(this.page, this.limit);
}