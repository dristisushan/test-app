

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smartmobe_test_app/bloc/hotelBloc/bloc.dart';
import 'package:smartmobe_test_app/model/Hotel/hotel_model.dart';
import 'package:smartmobe_test_app/network/api_provider.dart';


class HotelBloc extends Bloc<HotelEvent,HotelState>{
  List<Hotel> hotelList = [];
  @override
  // TODO: implement initialState
  HotelState get initialState => InitialHotelState();
  @override
  Stream<HotelState> mapEventToState(HotelEvent event) async* {
   if(event is GetHotels){
     yield* _getHotels(event.limit, event.page);
   }
  }

  Stream<HotelState> _getHotels(String limit,String page) async* {
    ApiProvider _apiProvider = ApiProvider();
    yield HotelStateWaiting();
    try {
      List<Hotel> data = await _apiProvider.getHotelList(page, limit);
      hotelList.addAll(data);
      yield GetHotelStateSuccess(hotelData: data);
    } catch (ex) {
      if (ex.message != 'cancel') {
        yield HotelStateError(errorMessage: ex.message.toString());
      }
    }
  }
}

