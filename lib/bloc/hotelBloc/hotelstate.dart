import 'package:meta/meta.dart';
import 'package:smartmobe_test_app/model/Hotel/hotel_model.dart';

@immutable
abstract class HotelState {}

class InitialHotelState extends HotelState {}

class HotelStateError extends HotelState {
  final String errorMessage;

  HotelStateError({
    this.errorMessage,
  });
}

class HotelStateWaiting extends HotelState {}

class GetHotelStateSuccess extends HotelState {
  final List<Hotel> hotelData;
  GetHotelStateSuccess({@required this.hotelData});
}