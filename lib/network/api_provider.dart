import 'package:smartmobe_test_app/base/constant.dart';
import 'package:smartmobe_test_app/base/utilities.dart';
import 'package:smartmobe_test_app/model/Hotel/hotel_model.dart';
import 'package:http/http.dart' as http;

class ApiProvider{
  Future<List<Hotel>> getHotelList(String page, limit) async {
    var uri = Uri(
      path: PHOTOS_URL,
      queryParameters: {
        "_page": page,
        "_limit": limit,
      },
    );
    Utilities.log("this is uri $BASE_URL$uri");
    var response = await http.get(
      BASE_URL + uri.toString(),
    );
    Utilities.log("this is response" + response.body.length.toString());
    List responseData = Utilities.decodeJson(response.body);
    List<Hotel> hotelList = Hotel.fromJsonArray(responseData);
    if (response.statusCode == 200) {
      return hotelList;
    } else {
      throw Exception("Exception");
    }
  }
}
